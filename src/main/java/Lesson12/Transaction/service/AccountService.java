package Lesson12.Transaction.service;

import Lesson12.Transaction.model.Account;
import Lesson12.Transaction.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountService {
    private final AccountRepository accountRepository;
    private final CacheManager cacheManager;
    //private final RedisTemplate<String, Object> redisTemplate;

    //    ------------------Redis Template----------------

//    public Account getById(Long id) {
//        var account = (Account) redisTemplate.opsForValue().get(String.valueOf(id));
//        if (account != null) {
//            return account;
//        }
//        Account account1 = accountRepository.findById(id).orElseThrow();
//        redisTemplate.opsForValue().set(String.valueOf(id),account1);
//        return account1;
//    }

    public Account create(Account account) {
        return accountRepository.save(account);
    }

    @SneakyThrows
//    @Cacheable(cacheNames = "account",key = "#id")
    public Account getById(Long id) {
        log.info("GetById method start");
        Account account = accountRepository.findById(id).get();
        log.info("GetById method end");
        return account;
    }

    @Cacheable(cacheNames = "account",key = "'all'")
    public List<Account> getAll() {
        return accountRepository.findAll();
    }


//    public Account getById(Long id) {
//        Cache cache = cacheManager.getCache("account");
//        Account accountFromCache = cache.get(id, Account.class);
//        if (accountFromCache == null) {
//            Account account = accountRepository.findById(id).get();
//            cache.put(id, account);
//            return account;
//        }
//        return accountFromCache;
//    }


    public Account update(Long id, Account account) {
        Cache cache = cacheManager.getCache("account");
        Account accountFromCache = cache.get(id, Account.class);

        if (accountFromCache == null) {
            accountFromCache = accountRepository.findById(id).get();
        }

        accountFromCache.setName(account.getName());
        accountFromCache.setBalance(account.getBalance());
        Account saveAccount = accountRepository.save(accountFromCache);

        cache.put(id, saveAccount);
        return saveAccount;
    }

    public Account delete(Long id) {
        Cache cache = cacheManager.getCache("account");
        Account accountFromCache = cache.get(id, Account.class);

        if (accountFromCache == null) {
            accountFromCache = accountRepository.findById(id).get();
        }

        accountRepository.delete(accountFromCache);

        cache.evict(accountFromCache.getId());
        return accountFromCache;
    }

//------------------------Cacheable------------------------------
//    @Cacheable(key = "#root.methodName", cacheNames = "account")
//    public List<Account> findAll() {
//        return accountRepository.findAll();
//    }


//    @Cacheable(cacheNames = "account", key = "#id")
//    public Account getById(Long id) {
//        log.info("GetById method start");
//        Account account = accountRepository.findById(id).get();
//        log.info("GetById method start");
//        return account;
//    }


//    @Transactional
//    @CachePut(key = "#account.id", cacheNames = "account")
//    public Account update(Account account) {
//        Account account1 = accountRepository.findById(account.getId()).orElseThrow();
//        account1.setName(account.getName());
//        account1.setBalance(account.getBalance());
//        return account1;
//    }
//
//    @CacheEvict(cacheNames = "account", key = "#id")
//    public Account delete(Long id) {
//        Account account = accountRepository.findById(id).orElseThrow();
//        accountRepository.delete(account);
//        return account;
//    }

}
