package Lesson12.Transaction.service;

import java.util.Scanner;

public class PalindromeWords {

    public static String checkPalindromeWords() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the words and check palindrome or not");
        String s = scanner.nextLine();
        StringBuilder b = new StringBuilder();
        b.append(s);
        String reverse = b.reverse().toString();
        if (s.equals(reverse)) {
            return "This word is Palindrome";
        } else return "This word isn't Palindrome";
    }
}
