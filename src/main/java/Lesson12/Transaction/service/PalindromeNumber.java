package Lesson12.Transaction.service;

import java.sql.SQLOutput;
import java.util.Scanner;

public class PalindromeNumber {

    public static String checkPalindrome() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number and check palindrome or not");
        int n=scanner.nextInt();
        int m=n,k=0;
        while(m>0){
            k = (k * 10) + (m % 10);
            m/=10;
        }
        if(n==k){
            return "Is palindrome";
        }else return "Isn't palindrome";
    }
}
