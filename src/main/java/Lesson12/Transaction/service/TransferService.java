package Lesson12.Transaction.service;

import Lesson12.Transaction.model.Account;
import Lesson12.Transaction.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransferService {
    private final AccountRepository accountRepository;

    //        ----------------------Payment----------------------
//    @SneakyThrows
//    @Transactional
//    public void transfer() {
//        log.info("Starting transfer on thread {}", Thread.currentThread().getId());
//        int amount = 30;
//        log.info("Getting account on thread {} with id 1", Thread.currentThread().getId());
//        Account from = accountRepository.getAccountById(1L);
//        log.info("Getting account on thread {} with id 1 DONE. {}", Thread.currentThread().getId(),from);
//        if (from.getBalance() < amount) {
//            throw new RuntimeException("balance not enough");
//        }
//        Thread.sleep(10000);
//        log.info("Getting account on thread {} with id 2", Thread.currentThread().getId());
//        Account to = accountRepository.getAccountById(2L);
//        log.info("Getting account on thread {} with id 2 DONE. {}", Thread.currentThread().getId(),to);
//
//        from.setBalance(from.getBalance() - amount);
//        to.setBalance(to.getBalance() + amount);
//        log.info("Balance on accounts on thread {} updated", Thread.currentThread().getId());
//
//        log.info("Trying to save account on thread {} with id 1", Thread.currentThread().getId());
//        accountRepository.save(from);
//        log.info("Account on thread {} with id 1 SAVED", Thread.currentThread().getId());
//
//
//        log.info("Trying to save account on thread {} with id 2", Thread.currentThread().getId());
//        accountRepository.save(to);
//        log.info("Account on thread {} with id 2 SAVED", Thread.currentThread().getId());

    //    }

    @SneakyThrows
    @Transactional
    public Account update() {
        log.info("Trying to get account");
        Account account = accountRepository.findById(4L).get();
        log.info("Trying to update balance");
        account.setBalance(account.getBalance() + 40);
        log.info("Trying to save account");
        Thread.sleep(10000);
        accountRepository.save(account);
        log.info("account save successful {}", account);
        return account;
    }

    @SneakyThrows
    @Transactional
    public Account update2() {
        log.info("Trying to get account #2");
        Account account = accountRepository.findById(4L).get();
        log.info("Trying to update balance #2");
        account.setBalance(account.getBalance() + 40);
        log.info("Trying to save account #2");
        accountRepository.save(account);
        log.info("account save successful {} #2", account);
        return account;
    }

    @Transactional
    public Account read() {
        log.info("Trying to read Account");
        Account account = accountRepository.findById(4L).get();
        log.info("Account read successfully {}", account);
        return account;
    }
}
