package Lesson12.Transaction.service;

import Lesson12.Transaction.model.PhoneNumber;
import Lesson12.Transaction.repository.PhoneNumberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PhoneService {
    private final PhoneNumberRepository phoneNumberRepository;


    @Cacheable(key = "#id")
    public PhoneNumber getPhoneNumber(Long id) {
        return phoneNumberRepository.findById(id).get();
    }
}
