package Lesson12.Transaction;

import Lesson12.Transaction.model.Account;
import Lesson12.Transaction.model.PhoneNumber;
import Lesson12.Transaction.repository.AccountRepository;
import Lesson12.Transaction.repository.PhoneNumberRepository;
import Lesson12.Transaction.service.AccountService;
import Lesson12.Transaction.service.GeneratePrimeNumber;
import Lesson12.Transaction.service.PalindromeNumber;
import Lesson12.Transaction.service.PalindromeWords;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
@EnableCaching
public class TransactionApplication implements CommandLineRunner {

    private final AccountRepository accountRepository;

    private final PhoneNumberRepository phoneNumberRepository;
    private final AccountService accountService;

    private final EntityManagerFactory emf;

    public static void main(String[] args) {
        SpringApplication.run(TransactionApplication.class, args);

    }

    @Override
//    @Transactional
    public void run(String... args) throws Exception {
//        ------------------Prime Numbers------------------
//        while (true) {
//            GeneratePrimeNumber.generatingPrimeNumber();
//            System.out.println();
//        }

//        ----------------Palindrome Numbers----------------
//        while (true){
//            System.out.println( PalindromeNumber.checkPalindrome());
//        }


//        ----------------------Palindrome Words-------------------
//        while (true) {
//            System.out.println(PalindromeWords.checkPalindromeWords());
//        }

    }
}

