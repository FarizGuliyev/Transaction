package Lesson12.Transaction.repository;

import Lesson12.Transaction.model.PhoneNumber;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneNumberRepository extends JpaRepository<PhoneNumber,Long> {
}
