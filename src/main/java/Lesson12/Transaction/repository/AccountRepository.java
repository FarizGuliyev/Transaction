package Lesson12.Transaction.repository;

import Lesson12.Transaction.model.Account;
import jakarta.persistence.LockModeType;
import lombok.NonNull;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNullApi;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {




    @Override
    @EntityGraph(attributePaths = "phoneNumberList")
    Optional<Account> findById(Long id);

    @EntityGraph(attributePaths = "phoneNumberList")
    List<Account> findAll();

//    @Override
//    @Lock(LockModeType.OPTIMISTIC_FORCE_INCREMENT)
//    Optional<Account> findById(Long id);
}
