package Lesson12.Transaction.controller;

import Lesson12.Transaction.model.Account;
import Lesson12.Transaction.model.PhoneNumber;
import Lesson12.Transaction.repository.AccountRepository;
import Lesson12.Transaction.service.AccountService;
import Lesson12.Transaction.service.PhoneService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/phone")
@RequiredArgsConstructor
public class PhoneNumberController {

    private final AccountRepository accountRepository;

    private final PhoneService phoneService;

    @GetMapping("/{id}")
    public PhoneNumber getPhoneNumber (@PathVariable Long id) {
        return phoneService.getPhoneNumber(id);
    }
}
