package Lesson12.Transaction.controller;

import Lesson12.Transaction.model.Account;
import Lesson12.Transaction.model.CarDto;
import Lesson12.Transaction.repository.AccountRepository;

import Lesson12.Transaction.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController {

    private final AccountRepository accountRepository;

    private final AccountService accountService;

    @PostMapping()
    public Account createAccount(@RequestBody Account account) {
        return accountService.create(account);
    }

    @GetMapping("/{id}")
    public Account getAccount(@PathVariable Long id) {
        return accountService.getById(id);
    }

    @GetMapping("/all")
    public List<Account> getAccount() {
        return accountService.getAll();
    }

    @PutMapping("/{id}")
    public Account updateAccount(@PathVariable Long id, @RequestBody Account account) {
        return accountService.update(id, account);
    }

    @DeleteMapping("/{id}")
    public Account deleteAccount(@PathVariable Long id) {
        return accountService.delete(id);
    }

}
