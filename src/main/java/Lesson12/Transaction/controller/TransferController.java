package Lesson12.Transaction.controller;

import Lesson12.Transaction.model.Account;
import Lesson12.Transaction.service.TransferService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/transfer")
@RequiredArgsConstructor
public class TransferController {

    private final TransferService transferService;

    @GetMapping("/update")
    public Account transfer() {
        return transferService.update();
    }

    @GetMapping("/update2")
    public Account transfer2() {
        return transferService.update2();
    }

    @GetMapping("/read")
    public Account read() {
        return transferService.read();
    }


}
