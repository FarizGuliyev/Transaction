package Lesson12.Transaction.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
class CalculatorTest {
    @InjectMocks
    private Calculator calculator;

    @Test
    void givenTwoDigitsWhenSumThenSuccess() {
        //Arrange
        int a = 5;
        int b = 10;

        //Act
        int sum = calculator.sum(a, b);
        //Assert
        assertThat(sum).isEqualTo(a + b);
    }
}